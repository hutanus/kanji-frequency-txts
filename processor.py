import csv
import os

data = list(csv.reader(open('kanji.csv', encoding='utf-8')))

allchars = []
temp = []

i = 0
for line in data[1::]:
    if i < 10:
        temp.append(line[2])
        i = i + 1
    else:
        i = 0
        allchars.append(''.join(temp))
        # print(len(temp))
        temp = []

if not os.path.isdir('Kanji txts'):
    os.makedirs('Kanji txts')

i = 1
for line in allchars:
    with open('Kanji txts/{}.txt'.format(str(i).zfill(3)), "w", encoding='utf-8') as text_file:
        text_file.write(line)
    print("Saved file {}".format(str(i).zfill(3)))
    i = i + 1